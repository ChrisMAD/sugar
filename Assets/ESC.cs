﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESC : MonoBehaviour {
    public GameObject[] Buttons;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 0)
            {
                ButtonClick();
            }
            else
            {
                Time.timeScale = 0;
                Buttons[0].SetActive(true);
                Buttons[1].SetActive(true);
            }
        }


	}
    public void ButtonClick()
    {
        Time.timeScale = 1;
        Buttons[0].SetActive(false);
        Buttons[1].SetActive(false);
    }
}
