﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class text : MonoBehaviour {
    public string texto;
    bool fliegen = false;
	// Use this for initialization
	void Start () {
        GetComponentInChildren<TextMesh>().text = texto;
       float x = GetWidth(GetComponentInChildren<TextMesh>());

        GetComponentsInChildren<Transform>()[2].transform.localScale = new Vector3(x, 1, 1);
        GetComponentsInChildren<Transform>()[2].transform.localPosition = new Vector3(x/2, 0, 0);
        GetComponentInChildren<MeshRenderer>().enabled = false;
        GetComponentInChildren<SpriteRenderer>().enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (fliegen)
        {
            transform.position = new Vector3(transform.position.x + Time.deltaTime * 0.3f, transform.position.y + Time.deltaTime*0.7f, transform.position.z);
        }
	}
    public static float GetWidth(TextMesh mesh)
    {
        float width = 0;
        foreach (char symbol in mesh.text)
        {
            CharacterInfo info;
            if (mesh.font.GetCharacterInfo(symbol, out info, mesh.fontSize, mesh.fontStyle))
            {
                width += info.advance;
            }
        }
        return width * mesh.characterSize * 0.1f;
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Spieler")
        {
            GetComponentInChildren<MeshRenderer>().enabled = true;
            GetComponentInChildren<SpriteRenderer>().enabled = true;
            fliegen = true;



        }
    }
}
