﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneAfterSec : MonoBehaviour {
    public int waitUntilLoad = 2;
    public string scene = "Level1";
	// Use this for initialization
	void Start () {
        StartCoroutine(Example(waitUntilLoad));
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(scene, LoadSceneMode.Single);
        }
	}
    IEnumerator Example(int x)
    {
        yield return new WaitForSeconds(x);
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
    
}
