﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wasser : MonoBehaviour {
    public bool cola;
    public AudioClip wasserplumpsen;
    private float lastPlay;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnTriggerEnter2D(Collider2D other)
    {

        if (Time.time > lastPlay + 2)
        {
            lastPlay = Time.time;
            GetComponent<AudioSource>().PlayOneShot(wasserplumpsen, 2F);
        }
       
    }
    public void OnTriggerStay2D(Collider2D other)
        
    {
        if (other.name == "Spieler")
        {
            if (cola)
                other.GetComponent<Movement>().cola = true;
            else
                other.GetComponent<Movement>().kaba = true;
        }

    }
    public void OnTriggerExit2D(Collider2D other)
    {
        if(other.name == "Spieler")
        {
            if (cola)
                other.GetComponent<Movement>().cola = false;
            else
                other.GetComponent<Movement>().kaba = false;

        }
    }
}
