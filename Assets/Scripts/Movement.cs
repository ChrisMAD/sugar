﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Movement : MonoBehaviour {

	public float speed;
    public float JumpForceMultiplierierGummy = 1.6f;
    public float jumpForceOrigin = 350;
    public float waterMultiplier = 0.2f;
    public float marschmellowWasserGravity = -0.5f;
    public float maxVelicotyMarschNachWasser = 0.4f;
    public AudioClip[] audios;
    public AudioSource[] aS;
  

    public GameObject colaJump;


    private float jumpForce = 350;
    public GameObject[] UI;
    public GameObject SpecialButtonUI;
    public Sprite[] SpecialButtonSprites;
    public bool grounded = false;
	private Animator animator;
	private Rigidbody2D rb2d;
    public bool dead = false;
	public GameObject zurckerwuerfelTod;
    public GameObject zurckerwuerfelTod2;
    public GameObject[] formen;
	public int aktiveForm = 0;
	public List<GameObject> wuerfelArray = new List<GameObject>();
	private bool mellowAcive = false;
    private bool mentosActive = false;
    private bool jellyActive = false;
    private bool weinActive = false;
    public Transform spawnPoint = null;
    private GameObject atm = null;
    private bool warimwasser = false;
    private bool colaJumpActive = false;
    private Vector3 movement = new Vector3(0,0,0);
    public int level = 1;
    public float maxvelo = 0;

    internal void respawn()
    {
        cola = false;
        kaba = false;
        this.transform.position = spawnPoint.position;
        rb2d.gravityScale = 1;
        this.GetComponentInChildren<SpriteRenderer>().enabled = true;
        //  GetComponent<SpriteRenderer>().sprite = null;
       
        dead = false;

    }

    public bool cola = false;
    public bool kaba = false;
    private float cameIntoWater;

    // Use this for initialization
    void Awake()
	{
        aS = GetComponents<AudioSource>();
        spawnPoint = transform;

	}
    public void nextLevel(String loadlevel)
    {
        GetComponentInChildren<Animator>().SetBool("Walk", false);
        GetComponentInChildren<Animator>().SetBool("Jump", false);
        dead = true;
        GetComponentInChildren<Camera>().transform.parent = null;
        StartCoroutine(nextLevelPort(50, loadlevel));
    }

    void Start()
	{	
		rb2d = GetComponent<Rigidbody2D>();
		aktiveForm = 0;

        atm = Instantiate(formen[0], this.transform.position, this.transform.rotation);
        atm.transform.parent = this.transform;
        if(level == 2)
        {
            activateForm(1);
            activateForm(2);
            activateForm(3);
        }

        if (level == 2)
        {
            this.transform.localScale = new Vector3(0, 0, 0);
            rb2d.gravityScale = 0;

            StartCoroutine(LevelPort(50));
        }else
        {
            GetComponentInChildren<Camera>().enabled = true;
        }
    }


	void Update()
	{

        /* if(maxvelo < rb2d.velocity.y)
         {
             maxvelo = rb2d.velocity.y;
         }
         */
        if (dead) return;
        if(rb2d.velocity.x  != 0)
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
        if (rb2d.velocity.y > 17.3)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, 17.3f);
        }

    	

		if (rb2d.velocity.y ==0.0) {
			grounded = true;
            GetComponentInChildren<Animator>().SetBool("Jump", false);
        } else {
            if(aktiveForm != 1)
			grounded = false;
         //   GetComponentInChildren<Animator>().SetBool("Jump", true);

        }

		movement = new Vector3 (0, 0,0);
        GetComponentInChildren<Animator>().SetBool("Walk", false);
        if (Input.GetKey("a") || Input.GetKey(KeyCode.LeftArrow))
        {
            if (aktiveForm == 1 && (cola || kaba))
                GetComponentInChildren<Animator>().SetBool("Walk", false);
            else
                GetComponentInChildren<Animator>().SetBool("Walk", true);


            atm.transform.rotation = Quaternion.Euler(0, 180, 0);
          //  this.transform.rotation = Quaternion.Euler(0, 180, 0);
            movement = new Vector3(-1, 0, 0);
        }
        if (Input.GetKey("d") || Input.GetKey(KeyCode.RightArrow))
        {
            if (aktiveForm == 1 && (cola || kaba))
                GetComponentInChildren<Animator>().SetBool("Walk", false);
            else
                GetComponentInChildren<Animator>().SetBool("Walk", true);
            movement = new Vector3(1, 0, 0);
            GetComponentInChildren<Animator>().SetBool("Walk", true);
            atm.transform.rotation = Quaternion.Euler(0, 0, 0);
        //    this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        

	
		if (Input.GetKeyDown ("e") && grounded) {
			if (aktiveForm == 0) {
                RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(0, 1), 4.2f);
                if (hit.collider != null)
                {
                //    print("HIT");
                }else
                {                                  
                    GameObject tmp = Instantiate(zurckerwuerfelTod, transform.position, transform.rotation);
                    transform.position += new Vector3(0, 2.2f, 0);
                    wuerfelArray.Add(tmp);
                    if (wuerfelArray.Count >= 4)
                    {
                        tmp = wuerfelArray[0];
                        wuerfelArray.Remove(tmp);
                        Destroy(tmp);
                    }
                }
                
                   
			}
		}
		if (Input.GetKey ("s")){}



        checkButtons();

        if (cola || kaba)
        {
            if(warimwasser == false)
            {
                cameIntoWater = Time.time;
            }
            if(Time.time > cameIntoWater + 1f)
            {
                aS[1].volume = 1;
                aS[0].volume = 0.25f;
            }
     
            warimwasser = true;
            
            if(aktiveForm == 2 )
            {
                if (cola)
                {
                    if (!colaJumpActive)
                    {
                        colaJumpActive = true;
                        StartCoroutine(startIdle());
                        GameObject tmp = Instantiate(colaJump, this.transform.position, this.transform.rotation);
                       // tmp.transform.parent = this.transform;
                        rb2d.gravityScale = 1;
                        rb2d.velocity = Vector2.zero;
                        rb2d.AddForce(new Vector2(0, jumpForceOrigin * 2.5f));
                    }
                }
                else
                {
                    rb2d.gravityScale = waterMultiplier;

                    jumpForce = jumpForceOrigin * waterMultiplier*2;
                }
            }
            if(aktiveForm == 0)
            {
                dead = true;
                rb2d.gravityScale = 0.05f;
                rb2d.velocity = Vector2.zero;
                Instantiate(zurckerwuerfelTod2, this.transform.position, this.transform.rotation);
                this.GetComponentInChildren<SpriteRenderer>().enabled = false;
                
                //  GetComponent<SpriteRenderer>().sprite = SpecialButtonSprites[0];
                StartCoroutine(Example());
                
                     

            }
            if(aktiveForm == 1)
            {
                GetComponentInChildren<Animator>().SetBool("Walk", false);
                rb2d.gravityScale = marschmellowWasserGravity;
            }
            if(aktiveForm >= 3)
            {
                rb2d.gravityScale = waterMultiplier;
                jumpForce = jumpForceOrigin * waterMultiplier*2;
            }
        }else
        {
            aS[0].volume = 1f;
            aS[1].volume = 0f;

            if (warimwasser)
            {
                warimwasser = false;
                if(aktiveForm == 1)
                {
                    if(rb2d.velocity.y > maxVelicotyMarschNachWasser)
                    {
                        grounded = true;
                        rb2d.velocity = new Vector2(rb2d.velocity.x, maxVelicotyMarschNachWasser);
                    }
                }
                    

            }
            rb2d.gravityScale = 1;
            jumpForce = jumpForceOrigin;
        }
  
    }

    void FixedUpdate()
    {
        if (dead) return;
        transform.position += movement * speed;
        if (Input.GetKey("w") && grounded || Input.GetKey("space") && grounded || Input.GetKey(KeyCode.UpArrow) && grounded)
        {
            GetComponentInChildren<Animator>().SetBool("Jump", true);
            StartCoroutine(startIdle());
            grounded = false;
        
            RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x-2, transform.position.y), new Vector2(0, 1), 2f);
            RaycastHit2D hit2 = Physics2D.Raycast(new Vector2(transform.position.x +2, transform.position.y), new Vector2(0, 1), 2f);
            RaycastHit2D hit3 = Physics2D.Raycast(new Vector2(transform.position.x , transform.position.y), new Vector2(0, 1), 2f);
            if (hit.collider == null && hit2.collider == null && hit3.collider == null )
            {
                if (aktiveForm % 2 == 0)
                    GetComponent<AudioSource>().PlayOneShot(audios[5], 0.2F);
                else
                    GetComponent<AudioSource>().PlayOneShot(audios[4], 0.2F);
            }
         

            if (aktiveForm == 4)
            {
                rb2d.AddForce(new Vector2(0, jumpForce * JumpForceMultiplierierGummy));
            }
            else
            {
                rb2d.AddForce(new Vector2(0, jumpForce));
            }

        }
      
            
       

    }
    IEnumerator nextLevelPort(int x,String loadlevel)
    {
        x -= 1;
       
       
        yield return new WaitForSeconds(0.1f);
        if(x == 0)
        {
            SceneManager.LoadScene(loadlevel, LoadSceneMode.Single);
        }else
        {
            this.transform.localScale -= new Vector3(0.04f,0.04f,0.04f) ;
            rb2d.gravityScale = -0.045f;
           
            
            StartCoroutine(nextLevelPort(x, loadlevel));
        }
    }
    IEnumerator LevelPort(int x)
    {
        x -= 1;
        dead = true;

        yield return new WaitForSeconds(0.1f);
        if (x == 0)
        {
            dead = false;
            rb2d.gravityScale = 1f;
            GetComponentInChildren<Camera>().enabled = true;
        }
        else
        {
           
            this.transform.localScale += new Vector3(0.04f, 0.04f, 0.04f);

            rb2d.gravityScale = 0.1f;
            StartCoroutine(LevelPort(x));
        }
    }
    IEnumerator Example()
    {
        yield return new WaitForSeconds(0.01f);
        this.GetComponentInChildren<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(3.9f);
        respawn(); 
    }
    IEnumerator startIdle()
    {
        yield return new WaitForSeconds(1);
        GetComponentInChildren<Animator>().SetBool("Jump", false);
        colaJumpActive = false;
    }







    private void checkButtons()
    {
   

        int formvorher = aktiveForm;
        if (Input.GetKeyDown("1"))
        {

            aktiveForm = 0;
          


        }
        if (Input.GetKeyDown("2") && mellowAcive)
        {
            aktiveForm = 1;


        }
        if (Input.GetKeyDown("3") && mentosActive)
        {
            aktiveForm = 2;
          
   

        }
        if (Input.GetKeyDown("4") && jellyActive)
        {
            aktiveForm = 3;
         


        }
        if (Input.GetKeyDown("5") && weinActive)
        {
            aktiveForm = 4;
 

        }
        if (aktiveForm != formvorher)
        {
            if (formvorher == 3)
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(0, 1), 1.9f);
                if (hit.collider != null)
                {
                 //   print(hit.distance);
                    aktiveForm = formvorher;
                    return;
                }
                else
                {
                 //   print(hit.distance);
                    print("nohit");
                }
            }
            //   rb2d.velocity = Vector2.zero;
            Quaternion tmp = atm.transform.rotation;
            Destroy(atm);
                atm = Instantiate(formen[aktiveForm], this.transform.position, tmp);
                atm.transform.parent = this.transform;
                GetComponentInChildren<Animator>().SetBool("Jump", false);
                GetComponentInChildren<Animator>().SetBool("Walk", false);
          



        }
        
        
        if(aktiveForm == 3)
        {
            GetComponent<BoxCollider2D>().size = new Vector2(0.4299067f, 0.7501822f);
            GetComponent<BoxCollider2D>().offset = new Vector2(0, 0.01f);
        }
        else
        {
            GetComponent<BoxCollider2D>().size = new Vector2(1.263f, 1.3f);
            GetComponent<BoxCollider2D>().offset = new Vector2(0, 0);
        }
        if(aktiveForm == 0)
        {
            SpecialButtonUI.GetComponent<Image>().sprite = SpecialButtonSprites[0];
        }else
        {
            SpecialButtonUI.GetComponent<Image>().sprite = SpecialButtonSprites[1];
        }

      
    }

    public void activateForm(int x){
        switch (x)
        {
           case 1: mellowAcive = true;
                break;
            case 2: mentosActive = true;
                break;
            case 3: jellyActive = true;
                break;
            case 4: weinActive = true;
                break;
        }
        UI[x].SetActive(true);		
	}
   
}