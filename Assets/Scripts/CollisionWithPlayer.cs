﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWithPlayer : MonoBehaviour {
    public int activateObject = 0;
    public Sprite[] Forms;


	// Use this for initialization
	void Start () {
        this.GetComponent<SpriteRenderer>().sprite = Forms[activateObject];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other){
		//UI.SetActive (true);
		other.GetComponent<Movement> ().activateForm (activateObject);
		Destroy (gameObject);
	}
}
