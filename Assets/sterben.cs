﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sterben : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(nextLevelPort(100));
      
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    IEnumerator nextLevelPort(int x)
    {
        x -= 1;


        yield return new WaitForSeconds(0.1f);
        if (x == 0)
        {
            Destroy(gameObject);
        }
        else
        {
                 foreach(Transform obj in gameObject.GetComponentsInChildren<Transform>())
                obj.transform.localScale -= new Vector3(0.01f, 0.01f, 0.01f);
            StartCoroutine(nextLevelPort(x));
        }
    }
}
