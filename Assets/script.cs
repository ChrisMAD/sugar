﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script : MonoBehaviour
{
    public GameObject spieler;
    public float minDistance = 1;
    public float maxDistance = 5;
    private float distance;
    private AudioSource audio;
    // Use this for initialization
    void Awake()
    {
        spieler = GameObject.Find("Spieler");
        distance = (this.transform.position - spieler.transform.position).magnitude;
        audio = this.GetComponent<AudioSource>();


    }

    // Update is called once per frame
    void Update()
    {
        distance = (this.transform.position - spieler.transform.position).magnitude;
        AdjustVolume();
    }

    private void AdjustVolume()
    {
        float distance = Mathf.Clamp(this.distance, minDistance, maxDistance);
        float volume = 1.0f - ((distance - minDistance) / (maxDistance - minDistance));

        audio.volume = volume;
    }
}